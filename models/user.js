const mongoose = require('mongoose');

let rolesValidos = {
  values: [
    'ADMIN_ROLE',
    "USER_ROLE"
  ],
  message: '{VALUE} no es un rol valido'
}

// User Schema
const usuarioSchema = new mongoose.Schema({
  nombre: {
    type: String,
    required: [true, "El nombre es necesario"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "El correo es necesario"],
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: [true, "El password es necesario"],
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    enum: rolesValidos
  },
  estado: {
    type: Boolean,
    default: true,
  },
  img: {
      type: String,
      required: false,
  },
  google: {
      type: Boolean,
      default: false
  }
});

module.exports = mongoose.model('Usuario', usuarioSchema);