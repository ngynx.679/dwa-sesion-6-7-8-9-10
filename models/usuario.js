const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let rolesValidos = {
    values: [
      'ADMIN_ROLE',
      "USER_ROLE"
    ],
    message: '{VALUE} no es un rol valido'
  }

let usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, "El nombre es necesario"],
    },
    email: {
        type: String,
        required: [true, "El email es necesario"],
    },
    password: {
        type: String,
        required: [true, "El password es necesario"],
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesValidos
    },
    estado: {
        type: Boolean,
        default: true,
    },
    img: {
        type: String,
        required: false,
    },
    google: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Usuario', usuarioSchema);