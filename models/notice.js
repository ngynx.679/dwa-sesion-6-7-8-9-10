const mongoose = require('mongoose');


const noticeSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  }
});

const Notice = module.exports = mongoose.model('Notice', noticeSchema);