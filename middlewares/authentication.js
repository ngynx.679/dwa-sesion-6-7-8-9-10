require('../config/config')
const jwt = require('jsonwebtoken');

const arreglo = require('../routes/routes');
//verificar token

let verificarToken = (req, res, next) => {
    let token = process.env.TOKEN;
    let logged = process.env.LOGGED;
    if (logged) {
        jwt.verify(token, process.env.SEED, (err, decoded) => {
            if (err) {
                return res.redirect('/api/error_401');
            }
            req.usuario = decoded.usuario;
            next();
            console.log(arreglo);
        });
    }else{
        res.json({
            ok:false,
        })
    }
};

module.exports = {
    verificarToken,
};
