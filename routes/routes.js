require('../config/config');

const express = require('express')

const mongoose = require('mongoose')

const app = express();

const bodyParser = require('body-parser');

const bcrypt = require('bcrypt');

const Usuario = require('../models/usuario');

const jwt = require('jsonwebtoken');
//Google OAuth library
const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

//middleware
const { verificarToken } = require('../middlewares/authentication');

app.use(express.static(__dirname + '/views'));
app.set('view engine', 'pug');

//
var tok = '';
var logged = false
//read
app.get('/usuario', verificarToken, function (req, res) {
    Usuario.find({}, function(err, noticias){
      if(err){
        console.error(err);
      } else {
        res.render('usuarios', {
          title: 'Usuarios Recientes', 
          noticias: noticias
        });
      }
    });
  });


//GET
app.get('/newuser', function(req, res){
    res.render('newuser', {
      title: 'New User'
    });
  });

//Agregar un nuevo user //GET and POST 
app.get('/registro', function(req, res){
    res.render('register', {
      title: 'Registro'
    });
  });
app.post('/newuser', function (req, res) {
    let body = req.body;

    let usuario = new Usuario({
       nombre: body.nombre,
       email: body.email,
       password: body.password,
       role: body.role, 
    });
    bcrypt.genSalt(10, function(err, salt){
        bcrypt.hash(usuario.password, salt, function(err, hash){
            if (err) {
                return res.json(400).json({
                    ok: false,
                    err,
                });
            }
            usuario.password = hash;
            usuario.save(function(err){
                if (err) {
                    console.log(err);
                    return err;
                } else {
                    res.redirect('/')
                }
            })
            
        })
    })
    

});


//put
app.get('/usuario/:id', function(req, res){
    Usuario.findById(req.params.id, function(err, usuario){
      res.render('user', {
        usuario: usuario
      });
    });
  });

app.get('/usuario/edit/:id', function(req, res){
    Usuario.findById(req.params.id, function(err, user){
      res.render('edit_user', {
        title: 'User Mod',
        user: user
      });
    });
  });
app.post('/usuario/edit/:id', verificarToken, function(req, res){
    let user = {};
    user.nombre = req.body.nombre;
    user.email = req.body.email;
    user.password = req.body.password;
  
    let query = {_id: req.params.id};
  
    Usuario.update(query, user, function(err){
      if(err) {
        console.error(err);
        return;
      } else {
        res.redirect('/');
      }
    })
  });

//delete
app.delete('/usuario/:id', function(req, res){
    let query = {_id: req.params.id};
  
    Usuario.remove(query, function(err){
      if(err) {
        console.error(err);
        return;
      } else {
        res.send('Success');
      }
    });
  });

//login sin TOKEN

app.get('/login', function(req, res){
    res.render('Login', {
      title: 'Nosotros'
    });
  });

app.post('/login', (req, res) => {
    
    let body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        if (!usuarioDB) {
            return res.status(400).json({
                ok:false,
                err: {
                    message: "Usuario incorrecto",
                },
            });
        }
        //Problemas con la contraseña
        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "contraseña incorrecta",
                },
            });
        }

        let token = jwt.sign({
            usuario: usuarioDB
        }, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN}) 

        res.redirect('/');

        logged = true;
        tok = token;
        //console.log(tok);
        process.env.TOKEN = token
        process.env.LOGGED = logged
        console.log(process.env.TOKEN)
    });
});

//error 401 token caduco o sin token

app.get('/error_401', function(req, res) {
    res.render('errortoken', {
        title: 'Token Expirado'
    })
})

//FORM
app.post('/send', function(req, res) {
  const title = req.body.title;
  const author = req.body.author;
  const description = req.body.description;
    
    res.render('recibido', {
        title: title,
        author: author,
        description: description
    })
})

//Google OAuth
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,
    });
    const payload = ticket.getPayload();

    console.log("Metodo verify");
    console.log(payload.name);
    console.log(payload.email);
    console.log(payload.picture);
    
    return payload
  }


app.post('/google', async (req, res) => {
    let token = req.body.idtoken;

    let googleUser = await verify(token).catch((e) => {
        return res.status(403).json({
            ok: false,
            err: e,
        });
    });
    
    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: "Debe autenticrse con la cuenta de la aplicacion",
                    },
                });
            } else {
                let token = jwt.sign(
                    {
                        usuario: usuarioDB,
                    },
                    process.env.SEED,
                    {
                        expiresIn: process.env.CADUCIDAD_TOKEN 
                    }
                );

                return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            }
        } else {
            //No hay usuario en la Base de datos

            let usuario = new Usuario();
            usuario.nombre = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = "123";

            usuario.save((err, usuarioDB) => {
                if (err) {
                    return res.status.json({
                        ok: false,
                        err,
                    });
                }
                let token = jwt.sign(
                    {
                        usuario: usuarioDB,
                    },
                    process.env.SEED,
                    {
                        expiresIn: process.env.CADUCIDAD_TOKEN
                    }
                );
                return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            });
        }
    });
    
});

module.exports = app, tok, logged;