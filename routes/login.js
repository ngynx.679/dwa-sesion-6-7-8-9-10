const express = require('express');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Usuario = require('../models/user');

const app = express();

//Google OAuth library
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);


app.post('/login', (req, res) => {
    let body = req.body;
    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        if (usuarioDB) {
            return res.status(400).json({
                ok:false,
                err: {
                    message: "Usuario incorrecto",
                },
            });
        }
        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "contraseña incorrecta",
                },
            });
        }
        res.json({
            ok:true,
            usuario: usuarioDB,
            token:'123',
        })
    });
});
//Google OAuth
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,
    });
    const payload = ticket.getPayload();

    console.log("Metodo verify");
    console.log(payload.name);
    console.log(payload.email);
    console.log(payload.picture);
    

  }


app.post('/google', (req, res) => {
    let token = req.body.idtoken;

    verify(token);

    res.json({
        token,
    });
})

module.exports = app;