const express = require("express");

const app = express();

app.use(express.static(__dirname + '/views'));


app.set('view engine', 'pug');
app.get("/", function(req, res){
    res.render('index', {
    })
});

app.get('/about', function(req, res){
    res.render('about', {
      title: 'Nosotros'
    });
  });
app.get('/servicios', function(req, res){
    res.render('servicios', {
      title: 'Services'
    });
  });

//Formulario
app.get('/send', function(req, res){
    res.render('recibido', {
      title: 'Services'
    });
  });
app.post('/send', function(req, res){
res.render('recibido', {
    title: 'Services'
});
});

//Google Prueba
app.get('/google', function(req, res) {
  res.render('google')
})


module.exports = app;