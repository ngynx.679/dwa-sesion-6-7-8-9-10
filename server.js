const express = require("express");

const app = express();
const port = process.env.PORT || 4000
const bodyParser = require('body-parser');

require('./config/config');
//DB
const mongoose = require('mongoose');
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

mongoose.connect(
  process.env.URLDB, 
  { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
  (err, res) => {
    if (err) throw err;

    console.log('Base de datos Online');
  }
);
  


//app.use(express.static(__dirname + '/public'));


const vistas = require('./routes/vistas');
app.use('/', vistas);
const routes = require('./routes/routes');
app.use('/api', routes);



app.listen(process.env.PORT, () => console.log(`escuchando peticiones en el puerto ${process.env.PORT}`));



///